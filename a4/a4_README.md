> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Steven Centeno

### Assignment 5 Requirements:

Deliverables:

1. Provide Bitbucketread-only access to lis4368repo, includelinks to the other assignment repos you created in README.md, using Markdownsyntax(README.mdmust also include screenshots as per aboveand below.)
2. Blackboard Links: https://bitbucket.org/sjc15d/lis4368

#### README.md file should include the following items:

* Screenshot of ERD that links to the image:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*A4 ERD Screenshot*

![Assignment 4 failed validation Screenshot](img/failed_validation.JPG)
![Assignment 4 successful validation Screenshot](img/successful_validation.JPG)


