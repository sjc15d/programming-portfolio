> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Steven Centeno

### Assignment # Requirements:

Three Points 

1. Distributed Version Control With Bitbucket and Git
2. Java/SP/Servlet Development Installation
3. Chapter Questions (Ch.1-4)

#### README.md file should include the following items:

* Screenshots of running Java Hello
* Screenshot of running http://localhost:9999
* git commands with short descriptions
* Bitbucket repo links a) this assignment and b)the completed tutorial above


> #### Git commands w/short descriptions:

1. git init - creates a new repository
2. git status - shows state of working directory and staging area
3. git add - updates the index with current content in the working tree
4. git commit - stores the current contents of the index in a new commit along with a log message from the user describing the changes
5. git push - updates remote refs using local refs, while sending objects necessary to complete the given refs
6. git pull - includes changes from a remote repository into the current branch
7. git rm - remove files from the index, or from the working tree and the index

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:9999

![Tomcat Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/sjc15d/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/sjc15d/myteamquotes/ "My Team Quotes Tutorial")
