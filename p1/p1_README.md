> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course Title
LIS4368
## Your Name
Steven Centeno
### Project 1 Requirements:

*Sub-Heading:*

1. Create a functional, complete images carousel
2. Add inputs for for client side validation program
3. Make sure the validation is successful when it should be and unsuccessful when it shouldnt be

#### README.md file should include the following items:

* Screenshot of complete image carousel
* Screenshot of successful client side validation
* Screenshot of unsuccessful client side validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Carousel on Main Page http://localhost*:

![Carousel Screenshot](img/carosel.PNG)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failed_validation.PNG)

*Screenshot of Successful Validation*:

![Successful Validation Screenshot](img/successful_validation.PNG)

