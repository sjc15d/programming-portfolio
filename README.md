> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Web Applications

## Steven Centeno

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    - Install JDK
    - Install tomcat
    * Screenshot of AMMPS running
    * Screenshot of Java Hello running
    * Screenshot of Android App
    
2. [A3 README.md](a3/a3_README.md "My A3 README.md file")

    * Entity Relationship Diagram
    * Include data (at least 10 records per table)
    * Provide Bitbucket read-only access to repo (Language SQL), must include README.md using markdown syntax, and include links to all of the following files (from README.md): docs folder a3.mwb and a3.sql img folder: a3.png (export a3.mwb file as a3.png) * README.md (MUST display a3.png ERD)
    * Blackboard Links: Bitbucket repo

3. [P1 README.md](p1/p1_README.md "My P1 README.md file")

    * Screenshot of complete image carousel
    * Screenshot of successful client side validation
    * Screenshot of unsuccessful client side validation

4. [A4 README.md](a4/a4_README.md "My A4 READ.md file")

    * Screenshot of failed validation
    * Screenshot of successful validation

5. [A5 README.md](a5/a5_README.md "My A5 README.md file")

    * Screenshot of Valid User Form Entry
    * Passed Validation
    * Associated Database Entry

6. [P2 README.md](p2/p2_README.md "My P2 README.md file")

    * Project 2 Screenshot of Valid User entry
    * Project 2 Passed Validation
    * Project 2 Display Data
    * Project 2 Add Customer
    * Project 2 Update Customer
    * Project 2 Modified Customer
    * Project 2 Delete Customer
    * Project 2 Associated Database Changes
