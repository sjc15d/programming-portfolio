> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Steven Centeno

### Project 2 Requirements:

Deliverables:

1. Provide Bitbucketread-only access to lis4368repo, includelinks to the other assignment repos you created in README.md, using Markdownsyntax(README.mdmust also include screenshots as per aboveand below.)
2. Blackboard Links: https://bitbucket.org/sjc15d/lis4368

#### README.md file should include the following items:

* Screenshot of valid user entry
* Screenshot of passed validation
* Screenshot of data being displayed
* Screenshot of customer being added
* Screenshot of customer being updated
* Screenshot of updated and added customer
* Screenshot of customer being deleted
* Screenshot of associated database changes

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*P2 Screenshots*

![Project 2 Screenshot of Valid User entry](img/p2_prevalidation.PNG "Valid user entry")
![Project 2 Passed Validation](img/post_validation.PNG "Passed validation")
![Project 2 Display Data](img/display_data.PNG "Display Data")
![Project 2 Add Customer](img/add_customer.PNG "Add customer")
![Project 2 Update Customer](img/update_customer.PNG "Update customer")
![Project 2 Modified Customer](img/updated_customer.PNG "Modified customer")
![Project 2 Delete Customer](img/delete_customer.PNG "Delete customer")
![Project 2 Associated Database Changes](img/adbc.PNG "Associated Database Changes")

