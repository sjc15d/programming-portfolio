> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Steven Centeno

### Assignment 3 Requirements:

Deliverables:

1. Entity Relationship Diagram
2. Include data (at least 10 records per table)
3. Provide Bitbucket read-only access to repo (Language SQL), must include README.md using markdown syntax, and include links to all of the following files (from README.md):
* docs folder a3.mwb and a3.sql
* img folder: a3.png (export a3.mwb file as a3.png)
* README.md (MUST display a3.png ERD)
4. Blackboard Links: Bitbucket repo

#### README.md file should include the following items:

* Screenshot of ERD that links to the image:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*A3 ERD Screenshot*

![Assignment 3 ERD Screenshot](img/a3_erd.png)



#### Assignment Links:

*A3 MWB File*
[A3 MWB File](docs/A3_ERD.mwb "This is my Assignment 3 MWB file")

*A3 SQL File*
[A3 SQL File](docs/A3_SQL_Statements_WEBAPPS.sql "This is my Assignment 3 SQL file")
