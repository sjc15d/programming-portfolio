-- A3_SQL_Statements for WEB APPS

--1 
select pst_id, pst_street, pst_city, pst_state, pst_zip, pst_phone
from petstore

--2
select pst_name, count(pet_id) as `number of pets`
from petstore
natural join pet
group by pst_id;

--3
select pst_id, cus_fname, cus_lname, cus_balance
from petstore
natural join pet 
natural join customer;

--4
update customer
set cus_lname='Valens'
where cus_id=2;

--5
delete from pet where pet_id=4;

--6 
insert into customer
VALUES
(null, 'John', 'Doe', '123 Main St.', 'Tallahassee','FL', 324035467, 8509871234, 'jdoe@aol.com', 101.67, 593.42, 'testing1'),
(null, 'Jane', 'Doe', '456 Oak St.', 'Chipley', 'FL', 327199164, 8506752645, 'jdoe@verizon.com', 242.19, 841.01, 'testing2');